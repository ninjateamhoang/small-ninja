<?php
/**
 * Plugin Name: Ninja Team Small Plugin
 * Plugin URI: https://ninjateam.org
 * Description: Internal template for plugin with simple logic
 * Version: 0.1.0
 * Author: Ninja Team
 * Author URI: https://ninjateam.org
 * Text Domain: small-ninja
 * Domain Path: /i18n/languages/
 *
 * @package SmallPlugin
 */

defined('ABSPATH') || exit;

define('SN_PREFIX', 'small-ninja');
define('SN_VERSION', '0.1.0');
define('SN_DOMAIN', 'small-ninja');

define('SN_PLUGIN_URL', plugin_dir_url(__FILE__));
define('SN_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('SN_INCLUDES_PATH', SN_PLUGIN_PATH . '/includes');

require_once SN_INCLUDES_PATH . '/plugin.php';
register_activation_hook(__FILE__, 'sn_activate');
register_deactivation_hook(__FILE__, 'sn_deactivate');
