<?php
defined('ABSPATH') || exit;

function sn_get_setting_page_id() {
  return SN_PREFIX . '-settings';
}

function sn_render_settings_page() {
  $view_path = SN_PLUGIN_PATH . 'views/pages/html-settings.php';
  include_once $view_path;
}

function sn_settings_menu() {
  add_submenu_page('options-general.php', __('Small Ninja Settings', SN_DOMAIN), __('Small Ninja Settings', SN_DOMAIN), 'manage_options', sn_get_setting_page_id(), 'sn_render_settings_page');
}

function sn_enqueue_settings_scripts($screen_id) {
  if ($screen_id === 'settings_page_small-ninja-settings') {
    $script_id = sn_get_setting_page_id();
    wp_enqueue_script($script_id, SN_PLUGIN_URL . 'assets/js/settings.js', array('jquery'), SN_VERSION);

    wp_localize_script($script_id, 'wpData', array(
      'test' => 'Ninja Team Test',
    ));
  }
}

function sn_add_settings_action_link($links) {
  $settings_links = array(
    '<a href="' . admin_url('options-general.php?page=' . sn_get_setting_page_id()) . '">Settings</a>',
  );
  return array_merge($settings_links, $links);
}

add_action('admin_menu', 'sn_settings_menu');
add_action('admin_enqueue_scripts', 'sn_enqueue_settings_scripts');

add_filter('plugin_action_links_small-ninja/small-ninja.php', 'sn_add_settings_action_link');
