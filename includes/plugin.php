<?php
defined('ABSPATH') || exit;

/** Require plugin logic */
function sn_plugin_init() {
  require_once SN_INCLUDES_PATH . '/i18n.php';
  require_once SN_INCLUDES_PATH . '/settings-page.php';
}

/** Plugin activated hook */
function sn_activate() {
}

/** Plugin deactivate hook */
function sn_deactivate() {
}

add_action('plugins_loaded', 'sn_plugin_init');