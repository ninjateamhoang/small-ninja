<?php
defined('ABSPATH') || exit;

function sn_load_plugin_textdomain() {
  load_plugin_textdomain(
    SN_DOMAIN,
    false,
    SN_PLUGIN_URL . 'i18n/languages/'
  );
}

add_action('plugins_loaded', 'sn_load_plugin_textdomain');
